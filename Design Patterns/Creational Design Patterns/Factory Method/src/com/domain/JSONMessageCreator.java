package com.domain;

import com.domain.message.JSONMessage;
import com.domain.message.Message;

public class JSONMessageCreator extends MessageCreator {

	@Override
	public Message createMessage() {
		return new JSONMessage();
	}

}
