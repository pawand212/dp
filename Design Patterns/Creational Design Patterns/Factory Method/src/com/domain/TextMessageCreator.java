package com.domain;

import com.domain.message.Message;
import com.domain.message.TextMessage;

public class TextMessageCreator extends MessageCreator {

	@Override
	public Message createMessage() {
		return new TextMessage();
	}

}
