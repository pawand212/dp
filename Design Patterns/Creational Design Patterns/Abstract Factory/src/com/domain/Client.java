package com.domain;

import com.domain.Instance.Capacity;

public class Client {

	private ResourceFactory factory;

	public Client(ResourceFactory factory) {
		this.factory = factory;
	}

	public Instance createServer(Instance.Capacity cp, int storageInMib) {
		Instance instance = factory.createInstance(cp);
		Storage storage = factory.createStorage(storageInMib);
		instance.attachStorage(storage);
		return instance;
	}

	public static void main(String[] args) {
		Client aws = new Client(new AwsResourceFactory());
		Instance i1 = aws.createServer(Capacity.micro, 20040);

		i1.start();
		i1.stop();

		System.out.println("***********************************************");

		Client gcp = new Client(new GoogleResourceFactory());
		Instance i2 = gcp.createServer(Capacity.large, 50000);

		i2.start();
		i2.stop();
	}
}
